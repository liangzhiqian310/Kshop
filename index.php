<?php
// 定义ThinkPHP框架路径(相对于入口文件)
define('THINK_PATH', './ThinkPHP/');

//定义项目名称和路径 必须一样
define('APP_NAME', 'Shop');
define('APP_PATH', './Shop/');

// 加载框架入口文件 
require(THINK_PATH."ThinkPHP.php");

//对编译缓存的内容是否进行去空白和注释
define("STRIP_RUNTIME_SPACE", false);

//不生成核心缓存文件
define("CACHE_RUNTIME", false);

//实例化一个网站应用实例
App::run();
?>
 