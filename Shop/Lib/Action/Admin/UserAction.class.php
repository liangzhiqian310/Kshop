<?php
header("Content-type:text/html; charset=utf-8");
class UserAction extends QxAction {
    protected $userModel;
    public function _initialize()
    {
        $this->feifa();
        $this->userModel = D('Admin.User');
    }

	//会员列表
	function index()
    {
		$data = $this->userModel->getUserListData();
		$this->assign('data', $data);
        $this->display();
	}

	//添加会员检测账号是否可用
	function user_check()
    {
        $condition['username'] = $_GET['username'];
        $status = $this->userModel->getUserCheckStatus($condition);
    	if(empty($status)){
    		$this->ajaxReturn('', '用户名不可注册~', 0);
    	}else{
    		$this->ajaxReturn($_GET['username'], '用户名可以注册~', 1);
    	}
	}

	//添加会员页面
	function add_user()
    {
		$this->feifa();
        $this->display('add_user');
	}

	//添加会员处理
	function add_user_sub()
    {
		$addData['username'] = $_POST['username'];
		$addData['password'] = md5($_POST['password']);
		$addData['email'] = $_POST['email'];
		$addData['add_time'] = time();
		$addData['userinfo']['gender'] = $_POST['gender'];
		$addData['userinfo']['birth_date'] = $_POST['birth_date'];
		$addData['userinfo']['phone'] = $_POST['phone'];
		$addData['userinfo']['mobile'] = $_POST['mobile'];
		$condition['username'] = array('eq', $_POST['username']);

        $status = $this->userModel->getAddUserStatus($condition, $addData);

        if($status)
        {
            $this->success('添加用户成功!');
        }
        else
        {
            $this->error('添加用户失败!');
        }
	}

	//编辑会员页面
	function edit_user()
    {
		$condition['id'] = array('eq', $_GET['id']);
        $data['userInfo'] = $this->userModel->getRelationFindUserData($condition);
		$this->assign('data',$data);
		$this->display('edit_user');
	}

	//编辑会员处理
	function edit_user_sub()
    {
		$saveData['id'] = $_POST['id'];
        if(!empty($_POST['password']))
        {
            $saveData['password'] = md5($_POST['password']);
        }
        $saveData['email'] = $_POST['email'];
        $saveData['userinfo']['name'] = $_POST['name'];
        $saveData['userinfo']['gender'] = $_POST['gender'];
		$saveData['userinfo']['birth_date'] = $_POST['birth_date'];
        $saveData['userinfo']['sel0'] = $_POST['sel0'];
        $saveData['userinfo']['sel1'] = $_POST['sel1'];
        $saveData['userinfo']['sel2'] = $_POST['sel2'];
        $saveData['userinfo']['site'] = $_POST['site'];
        $saveData['userinfo']['zip_code'] = $_POST['zip_code'];
        $saveData['userinfo']['mobile'] = $_POST['mobile'];
        $saveData['userinfo']['phone'] = $_POST['phone'];
        $saveData['userinfo']['question'] = $_POST['question'];
        $saveData['userinfo']['answer'] = $_POST['answer'];

        $status = $this->userModel->getSaveUserDataStatus($saveData);

        if($status)
        {
			$this->success('修改会员信息成功!');
		}
        else
        {
			$this->error('修改会员信息失败!');
		}
	}

	//删除会员
	function del_user()
    {
		isset($_GET) ? $deleteId = implode(',', $_GET) : 0;
        $status = $this->userModel->getDeleteUserStatus($deleteId);
		if($status)
        {
		    $this->success('删除会员成功!');
	    }
        else
        {
		    $this->error('删除会员失败!');
		}
	}

    //搜索会员
    function search_user()
    {
        $gt = $_POST['pay_points_gt'];
        $lt = $_POST['pay_points_lt'];
        $keyword = $_POST['keyword'];
        $data = $this->userModel->getSearchUserData($gt, $lt, $keyword);
        $this->assign('data', $data);
        $this->display('index');
    }
}
?>