<?php
header("Content-type:text/html; charset=utf-8");
class AdminAction extends QxAction {
    protected $adminModel;
    public function _initialize()
    {
        $this->feifa();
        $this->adminModel = D('Admin.Admin');
    }

	//管理员列表
	function index()
    {
        $data['list'] = $this->adminModel->getAdminListData();
		$this->assign('data', $data);
		$this->display();
	}

    //编辑管理员页面
    function edit_admin()
    {
        $condition['id'] = array('eq', $_GET['id']);
        $data['info'] = $this->adminModel->getAdminFindData($condition);
        $this->assign('data', $data);
        $this->display('edit_admin');
    }

    //编辑管理员处理
    function edit_admin_sub(){
        $condition['id'] = $_POST['id'];
        $saveData['email'] = $_POST['email'];

        if(!empty($_POST['password']))
        {
            $saveData['password'] = md5($_POST['password']);
        }

        $status = $this->adminModel->getSaveAdminDataStatus($condition, $saveData);

        if(!empty($status))
        {
            $this->success('修改管理员资料成功');
        }
        else
        {
            $this->error('修改管理员资料失败');
        }
    }

	//添加管理员页面
	function add_admin(){
		$this->display('add_admin');
	}

	//添加管理员处理
	function add_admin_sub(){
        $addData['username'] = $_POST['username'];
        $addData['password'] = md5($_POST["password"]);
        $addData['email'] = $_POST['email'];
        $addData['addtime'] = time();
        $addData['logintime'] = 0;

        $condition['username'] = array('eq', $_POST['username']);

        $status = $this->adminModel->getAddAdminDataStatus($condition, $addData);

        if($status['userExist'])
        {
            $this->error('需要添加的用户名已存在');
        }
        else if(!empty($_POST['username']) && !empty($_POST['password']))
        {
            if($status['addUser'])
            {
                $this->success('添加管理员成功');
            }
            else
            {
                $this->error('添加管理员失败');
            }
        }
        else
        {
            $this->error('用户名或密码为空');
        }
	}
	//删除管理员
	function del_admin(){
        $condition['id'] = $_GET['id'];
        $stauts = $this->adminModel->getDeleteAdminStatus($condition);

        if($stauts)
        {
	        $this->success('删除管理员成功!');
		}
        else
        {
			$this->error('删除管理员失败!');
		}
	}
	
	
}
?>