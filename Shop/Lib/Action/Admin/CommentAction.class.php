<?php
class CommentAction extends QxAction
{
    protected $commentModel;
    public function _initialize()
    {
        $this->feifa();
        $this->commentModel = D('Admin.Comment');
    }

	function index()
    {
        $data = $this->commentModel->getCommentListData();
        $this->assign('data', $data);
		$this->display();
	}

	//添加商品评论
	function add_comment()
    {
		$this->display();
	}

	//添加商品评论处理
	function add_comment_sub()
    {
		$_POST['add_date'] = time($_POST['add_date']);
		$_POST['reply_date'] = time($_POST['reply_date']);
		if($this->commentModel->getAddCommentDataStatus($_POST))
        {
			$this->success('添加商品评论成功!');

		}
        else{
			$this->error('添加商品评论失败!');
		}
	}


	//编辑商品评论
	function edit_comment()
    {
        $commentCondition['id'] = array('eq', $_GET['id']);
		$data['commentInfo'] = $this->commentModel->getFindCommentData($commentCondition);

        $goodsTitleCondition['id'] = array('eq', $data['commentInfo']['goods_id']);
        $data['goodsTitle'] = $this->commentModel->getGoodsTitleData($goodsTitleCondition);

		$this->assign('data', $data);
		$this->display('edit_comment');
	}

	//编辑商品评论处理
	function edit_comment_sub()
    {
		$comment = M("comment");
		$_POST["add_date"] = time($_POST["add_date"]);
		$_POST["reply_date"] = time($_POST["reply_date"]);
		if($comment->where("id =".$_POST["where_id"])->save($_POST))
        {
			$this->success("修改评论成功!");
		}
        else
        {
			$this->error("修改评论失败!");
		}
	}

	//删除评论
	function del_comment()
    {
		isset($_GET) ? $deleteId = implode(',', $_GET) : 0;
		if($this->commentModel->getDeleteCommentDataStatus($deleteId))
        {
			$this->assign('waitSecond', 3);
			$this->success('删除评论成功!');
		}
        else
        {
			$this->error('删除评论失败!');
		}
	}
	
}
?>