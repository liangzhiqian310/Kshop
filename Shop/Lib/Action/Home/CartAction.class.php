<?php
class CartAction extends CommonAction
{
    protected $cartModel;
    public function _initialize()
    {
        $this->cartModel = D('Home.Cart');
        $webInfo = $this->cartModel->webInfo();
        $footerNews = $this->cartModel->webFooterNews();
        $this->assign('webInfo', $webInfo);
        $this->assign('footerNews', $footerNews);
    }

	function index()
    {
        //验证是否登录
        Cookie::get('feifa_home') == "passageway_home" ? $condition['qx'] = Cookie::get('feifa_home') == "passageway_home" : 0;
        Cookie::get('user_name') ? $condition['userName'] = Cookie::get('user_name') : 0;
        cookie::get('user_id') ? $condition['userId'] = Cookie::get('user_id') : 0;
        $condition['goods_id'] = $_POST['goods_id'];
        $condition['buy_num']= $_POST['buy_num'];

        $result = $this->cartModel->getCartIndexData($condition);
        if($result['loginStatus'] == 1)
        {
            if($result['addStatus'] == 1)
            {
                $this->redirect('cart');
            }
            else
            {
                $this->error(C('ERROR_OPERATION_FAILURE'));
            }
        }
        else
        {
            $this->error(C('ERROR_NOT_LOGIN_NOT_BUY'));
        }
	}

	//查看购物车
	function cart()
    {
		Cookie::get('feifa_home') == "passageway_home" ? $qx = Cookie::get('feifa_home') == "passageway_home" : 0;
		Cookie::get('user_name') ? $user_name = Cookie::get('user_name') : 0;
        if(isset($qx) && isset($user_name))
        {
			$condition['user_id'] = array('eq', Cookie::get('user_id'));
            $data = $this->cartModel->getCartStatusData($condition);
            $this->assign('data', $data);
		}
        else
        {
			$this->error(C('ERROR_CART_EMPTY'));
		}
		$this->display('cart');
	}

	//AJAX更新数量和总价
	function up_ajax()
    {
        $result = $this->cartModel->getUpdateCartAjaxData($_GET);
        if($result['ajaxReturn'] == 1)
        {

            $this->ajaxReturn($result, C('SUCCESS_UPDATE_CART_INFO_SUCCESS'), 1);
        }
        else if($result['ajaxReturn'] == -1)
        {
            $this->ajaxReturn($result, C('ERROR_CART_INVENTORY_LACK'), -1);
        }
        else
        {
            $this->ajaxReturn('', C('ERROR_UPDATE_CART_INFO_FAILURE'), 0);
        }
	}

	//删除购物车商品
	function del_goods()
    {
		$condition['id'] = array('eq', $_GET['condition_id']);
        $status = $this->cartModel->getDeleteGoodsStatus($condition);
        if($status)
        {
			$this->ajaxReturn($condition['id'], C('SUCCESS_DELETE_CART_GOODS_SUCCESS'), 1);  //删除成功
		}
        else
        {
			$this->ajaxReturn('', C('ERROR_DELETE_CART_GOODS_FAILURE'), 0);           //删除失败
		}
	}

	//清空购物车
	function empty_cart()
    {
		$condition['user_id'] = array('eq', Cookie::get('user_id'));
        $status = $this->cartModel->getClearCartStatus($condition);
        if($status)
        {
			$this->success(C('SUCCESS_CLEAR_CART_SUCCESS'));
		}
        else
        {
			$this->error(C('ERROR_CLEAR_CART_FAILURE'));
		}
	}

	//购物车优惠券
	function coupon()
    {
        if((empty($_POST['coupon_code'])) || ($_POST['coupon_code'] == '请输入优惠券号码'))
        {
			$this->error(C('ERROR_COUPON_CODE_EMPTY'));
		}

		//测试专用优惠码，1分钱购买
		if($_POST['coupon_code'] == 'xujiantao')
        {
			//打折处理
			$condition['user_id'] = array('eq', Cookie::get('user_id'));
            $result = $this->cartModel->setCouponPrice($condition);

			//打折成功
			if(count($result['success']) == count($result['cart_list']))
            {
				$this->success(C('SUCCESS_TEST_COUPON_CODE_SUCCESS'));
			}
            else
            {
				$this->error(C('ERROR_TEST_COUPON_CODE_FAILURE'));
			}
		}

		//商用优惠码处理
		$couponCodeCondition['coupon_code'] = array('eq', $_POST['coupon_code']);
        $couponCode = $this->cartModel->getSearchCouponStatus($couponCodeCondition);
        if(!empty($couponCode))
        {
            $result = $this->cartModel->getCouponBizStatus($couponCode);
            //打折成功
            if(intval(count($result['couponCode'])) == intval(count($result['cart_list'])))
            {
                $this->success(C('SUCCESS_DISCOUNT_SUCCESS'));
            }
            else
            {
                $this->error(C('ERROR_DISCOUNT_FAILURE'));
            }
		}
        else
        {
			$this->error(C('ERROR_COUPON_CODE_ERROR'));
		}

	}

	//填写购物信息
	function cart_checkout()
    {
		$this->feifa();
        $condition['user_id'] = array('eq', Cookie::get('user_id'));
		$data['cartList'] = $this->cartModel->getUserCartInfo($condition);
        $data['title'] = "Kshop数码,数码相机,手机商城,电脑商城,全国货到付款！  ";

		if(empty($data['cartList']))
        {
			$this->assign("jumpUrl","cart");
			$this->error(C('ERROR_CART_EMPTY'));
		}
        else
        {
            $data = $this->cartModel->getUserCartInfoSupplement($data);
		}
		$this->assign('data',$data);
		$this->display("cart_checkout");
	}

	//下单成功
	function add_orders()
    {
        $result = $this->cartModel->getAddOrdersStatus();
        if($result['status'])
        {
            header('Location:'.U('Home-Pay/aliPayTo/id/').$result['orderId']);
		}
        else
        {
			$this->error(C('ERROR_ORDER_FAILURE'));
		}
	}

	//付款成功
	function cart_success()
    {
        $condition['order_No'] = array('eq', $_GET['orderno']);
		$data = $this->cartModel->getPaymentSuccess($condition);
        $this->assign('data', $data);
		$this->display('cart_success');
	}

	//订单详情页面
	function cart_orders_details()
    {
		$condition['id'] = array('eq', $_GET['id']);
        $data = $this->cartModel->getOrderDetailsData($condition);
        $this->assign('data', $data);
		$this->display('cart_orders_details');
	}
}
?>