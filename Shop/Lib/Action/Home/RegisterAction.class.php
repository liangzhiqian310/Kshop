<?php
class RegisterAction extends CommonAction {
    protected $registerModel;
    public function _initialize()
    {
        $this->registerModel = D('Home.Register');
        $webInfo = $this->registerModel->webInfo();
        $footerNews = $this->registerModel->webFooterNews();
        $this->assign('webInfo', $webInfo);
        $this->assign('footerNews', $footerNews);
    }

	//注册
    function index()
    {
    	$this->display();
    }

    //检测用户名是否被注册
    function register_user()
    {
    	$get = trim($_GET['username']);
		$get = preg_replace('/\s(?=\s)/', '', $get);
    	$condition['username'] = array('eq', $get);
        $status = $this->registerModel->getUserCheckStatus($condition);
        if(empty($status))
        {
    		$this->ajaxReturn('', C('ERROR_ACCOUNT_NOT_REGISTER'), 0);
    	}
        else
        {
    		$this->ajaxReturn($condition['username'], C('SUCCESS_ACCOUNT_CAN_REGISTER'), 1);
    	}
     
    }

    //验证添加注册用户
    function register_sub()
    {
    	unset($_POST['pwd_confirm']);
    	unset($_POST['favorite']);

    	if(md5($_POST['code']) != $_SESSION['verify'])
        {
    		$this->error(C('ERROR_VERIFY_ERROR'));
    	}
        else
        {
    		$_POST['password'] = md5($_POST['password']);
    		$_POST['add_time'] = time();
            $checkUsernameCondition['username'] = array('eq', $_POST['username']);
            $status = $this->registerModel->getUserCheckStatus($checkUsernameCondition);

            if(empty($status))
            {
                $saveData['username'] = trim($_POST['username']);
                $saveData['password'] = md5($_POST['password']);
                $saveData['email'] = trim($_POST['email']);
                $saveData['add_time'] = time();
                $userResult = $this->registerModel->getRigisterUser($saveData);
                if($userResult['status'])
                {
                    Cookie::set('user_name', $_POST['username']);
                    Cookie::set('user_id', $userResult['user_id'], 60*60*24); //user id
                    Cookie::set('feifa_home', 'passageway_home', 60*60*24); //cookie 验证是否登录
                    Cookie::set('cart_num', $userResult['cart_num'], 60*60*24);  // 设置cookie购物车商品数

                    $email = trim($_POST['email']);
                    $title = '感谢注册您的 Kshop数码 ！';
                    $content = '<div>';
                    $content .= sprintf('尊敬的&nbsp;%s<br>', COOKIE::get('user_name'));
                    $content .= '感谢您注册 Kshop数码，您的个人信息请妥善保管个人注册信息<br>';
                    $content .= sprintf('用户名：%s<br>发送时间：%s<br>', $_POST['username'], date('Y-m-d H:i:s',$_POST['add_time']));
                    $content .= '■重要信息：由于此邮件包含个人注册资料，请妥善保存!</div>';

                    //注册成功
                    $this->SendMail($email, $title, $content);
                    $this->redirect('passport_create',array('user_id'=>$userResult['userId']));
                }
                else
                {
                    $this->error(C('ERROR_REGISTER_FAILURE'));
                }

            }
            else
            {
            	$this->error(C('ERROR_ACCOUNT_HAVE_USE'));
            }

    	}
    	
    }

    //注册成功,填写收货地址，手机等信息
    function passport_create()
    {
    	$this->assign('user_id', $_GET['user_id']);
    	$this->display('passport_create');
    }

    //填写完毕收货等信息
    function saveuser_info()
    {
        $status = $this->registerModel->getSaveUserInfo($_POST);
        if($status)
        {
            $this->assign('jumpUrl', U('Home-Member/index'));
            $this->assign('waitSecond', 3);
            $this->success(C('SUCCESS_SUBMIT_USER_INFO_SUCCESS'));
            //提交成功，跳到会员中心
        }
        else
        {
            $this->error(C('ERROR_SUBMIT_USER_INFO_FAILURE'));
        }
    }
}
?>