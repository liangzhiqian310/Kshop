<?php
/**
 * Created by PhpStorm.
 * User: xujiantao
 * Date: 13-11-19
 * Time: 16:31
 */
class GoodslistModel extends BaseModel
{
    protected $thisGoodsSort;
    public function _initialize()
    {
        $this->thisGoodsSort = $this->goodsSort();
    }

    public function getGoodslistIndexData($id)
    {
        import('@.ORG.Page');
        $condition['brand'] = array('eq', $id);  //查找品牌相关的商品
        $condition['putaway'] = array('eq', 1);  //查找上架的商品
        $condition['_logic'] = 'and';

        $count = $this->tableGoods()->where($condition)->field('id')->count();
        $page = new Page($count, 10); // 实例化分页类 传入总记录数和每页显示的记录数
        $page->setConfig('header', '个商品');
        $page->setConfig('theme', '<span class="pagestyle" style="color:blue">共%totalRow%%header%</span> <span class="pagestyle">当前%nowPage%&nbsp;/&nbsp;%totalPage% 页</span> %first% %upPage%  %linkPage% %downPage% %end%');
        $result['goods_list'] = $this->tableGoods()->where($condition)->field('id, title, title_info, inventory, sell_price, pic_one, path')->order('id desc')->limit($page->firstRow.','.$page->listRows)->select();
        $result['show'] = $page->show(); // 分页显示输出
        $result = array_merge($this->thisGoodsSort, $result);
        return $result;
    }
}