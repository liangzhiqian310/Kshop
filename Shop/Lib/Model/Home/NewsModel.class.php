<?php
/**
 * Created by PhpStorm.
 * User: xujiantao
 * Date: 13-11-19
 * Time: 20:07
 */
class NewsModel extends BaseModel
{
    public function getIndexNewsData()
    {
        $data['list'] = $this->tableNews()->where("id =".$_GET["id"])->find();
        $data['news_list'] = $this->tableNews()->order("id asc")->field("id,news_title")->select();
        return $data;
    }

    public function getNewsListData()
    {
        import("@.ORG.Page");
        $count = $this->tableNews()->count();
        $page = new Page($count,15);
        $page->setConfig('header', '条新闻');
        $page->setConfig('theme', '<span class="pagestyle" style="color:blue">共%totalRow%%header%</span> <span class="pagestyle">当前%nowPage%&nbsp;/&nbsp;%totalPage% 页</span> %first% %upPage%  %linkPage% %downPage% %end%');
        $data['list'] = $this->tableNews()->order('id desc')->limit($page->firstRow.','.$page->listRows)->select();
        $data['show'] = $page->show();
        $data['news_list'] = $this->tableNews()->order('id asc')->field('id,news_title')->select();
        return $data;
    }
}
?>