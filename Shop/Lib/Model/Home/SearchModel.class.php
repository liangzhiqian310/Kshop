<?php
/**
 * Created by PhpStorm.
 * User: xujiantao
 * Date: 13-11-19
 * Time: 20:16
 */
class SearchModel extends BaseModel
{
    protected $thisGoodsSort;
    public function _initialize()
    {
        $this->thisGoodsSort = $this->goodsSort();
    }

    public function getSearchData($condition)
    {
        import('@.ORG.Page');
        $count = $this->tableGoods()->where($condition)->count();
        $page = new Page($count, 10);
        $page->setConfig('header', '个商品');
        $page->setConfig('theme', '<span class="pagestyle" style="color:blue">共%totalRow%%header%</span> <span class="pagestyle">当前%nowPage%&nbsp;/&nbsp;%totalPage% 页</span> %first% %upPage%  %linkPage% %downPage% %end%');
        $result['goods_list'] = $this->tableGoods()->where($condition)->order('id desc')->limit($page->firstRow.','.$page->listRows)->select();
        $result['goods_num'] = $count;
        $result['show'] = $page->show();
        $data = array_merge($this->thisGoodsSort, $result);
        return $data;
    }

    public function getGoodsSortData()
    {
        return $this->thisGoodsSort;
    }
}
?>