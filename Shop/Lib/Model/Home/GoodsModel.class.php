<?php
/**
 * Created by PhpStorm.
 * User: xujiantao
 * Date: 13-11-18
 * Time: 21:11
 */
class GoodsModel extends BaseModel
{
    protected $thisGoodsSort;
    public function _initialize()
    {
        $this->thisGoodsSort = $this->goodsSort();
    }

    public function getGoodsIndexData($id)
    {
        import('@.ORG.Page');
        // 商品参数
        $result['g_list'] = $this->tableGoods()->where("id =".$id)->find();
        $result['location'][1] = $this->tableSort()->where("id =".$result['g_list']["brand"])->field("pid,name")->find();
        $result['location'][0] = $this->tableSort()->where("id =".$result['location'][1]["pid"])->field("id,name")->find();

        // 遍历相关评论
        $commentCondition['goods_id'] = array('eq', $_GET['id']);
        $commentCondition['show'] = array('eq', 1);
        $commentCondition['_logic'] = 'and';

        $count = $this->tableComment()->where($commentCondition)->count();
        $page = new Page($count, 5); // 实例化分页类 传入总记录数和每页显示的记录数
        $page->setConfig('header', '条咨询');
        $page->setConfig('theme', '<span class="pagestyle" style="color:blue">共%totalRow%%header%</span> <span class="pagestyle">当前%nowPage%&nbsp;/&nbsp;%totalPage% 页</span> %first% %upPage%  %linkPage% %downPage% %end%');
        $result['commentData'] = $this->tableComment()->where($commentCondition)->order('id desc')->limit($page->firstRow.','.$page->listRows)->select();
        $result['show'] = $page->show();
        $result = array_merge($this->thisGoodsSort, $result);
        return $result;
    }

    public function getAddGoodsCommentDataStatus($addData)
    {
        return $this->tableComment()->add($addData);
    }
}