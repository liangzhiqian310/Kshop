<?php
class UserModel extends BaseModel
{
    public function getUserListData()
    {
        import("@.ORG.Page");
        $count = $this->tableUser()->count();
        $page = new Page($count,10);
        $page->setConfig('header', '个会员');
        $page->setConfig('theme', '<span class="pagestyle" style="color:blue">共%totalRow%%header%</span> <span class="pagestyle">当前%nowPage%&nbsp;/&nbsp;%totalPage% 页</span> %first% %upPage%  %linkPage% %downPage% %end%');
        $result['list'] = $this->tableUser()->order('id desc')->limit($page->firstRow.','.$page->listRows)->select();
        $result['show'] = $page->show();
        return $result;
    }

    public function getUserCheckStatus($condition)
    {
        return $this->tableUser()->where($condition)->find();
    }

    public function getAddUserStatus($condition, $addData)
    {
        return $this->tableUser()->relation(true)->where($condition)->add($addData);
    }

    public function getRelationFindUserData($condition)
    {
        return $this->tableUser()->relation(true)->where($condition)->find();
    }

    public function getSaveUserDataStatus($saveData)
    {
        return $this->tableUser()->relation(true)->save($saveData);
    }

    public function getDeleteUserStatus($deleteId)
    {
        return $this->tableUser()->relation(true)->delete($deleteId);
    }

    public function getSearchUserData($gt, $lt, $keyword)
    {
        import('@.ORG.Page'); // 导入分页类

        if(!empty($gt))
        {
            $condition['sum_points'] = array('gt',$gt);
        }
        if(!empty($lt))
        {
            $condition['sum_points'] = array('lt',$lt);
        }
        if(!empty($keyword))
        {
            $condition['username'] = array('like','%'.$keyword.'%');
        }

        $count = $this->tableUser()->where($condition)->count();
        $page = new Page($count,10);
        $result['list'] = $this->tableUser()->where($condition)->order('id desc')->limit($page->firstRow.','.$page->listRows)->select();

        $result['searchInfo']['gt'] = $gt;
        $result['searchInfo']['lt'] = $lt;
        $result['searchInfo']['keyword'] = $keyword;

        $page->setConfig('header', '个会员');
        $page->setConfig('theme', '<span class="pagestyle" style="color:blue">共%totalRow%%header%</span> <span class="pagestyle">当前%nowPage%&nbsp;/&nbsp;%totalPage% 页</span> %first% %upPage%  %linkPage% %downPage% %end%');
        $result['show'] = $page->show();
        return $result;
    }
}
?>