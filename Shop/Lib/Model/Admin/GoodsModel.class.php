<?php
/**
 * Created by PhpStorm.
 * User: xujiantao
 * Date: 13-11-28
 * Time: 18:54
 */
class GoodsModel extends BaseModel
{
    public function getGoodsListData()
    {
        import('@.ORG.Page');
        $count = $this->tableGoods()->count();
        $page = new Page($count,12);
        $page->setConfig('header', '件商品');
        $page->setConfig('theme', '<span class="pagestyle" style="color:blue">共%totalRow%%header%</span> <span class="pagestyle">当前%nowPage%&nbsp;/&nbsp;%totalPage% 页</span> %first% %upPage%  %linkPage% %downPage% %end%');
        $result['list'] = $this->tableGoods()->order('id desc')->limit($page->firstRow.','.$page->listRows)->select();
        $result['show'] = $page->show();
        return $result;
    }

    public function getAllSortListData()
    {
        $result['sortList'] = $this->tableSort()->field('id,name,pid,path,concat(path,"-",id) as bpath')->order('bpath')->select();

        foreach($result['list'] as $key=>$value)
        {
            $result['sortList'][$key]['count'] = count(explode('-', $value['bpath']))-2;
        }
        return $result;
    }

    public function getAddGoodsDataStatus($addData)
    {
        return $this->tableGoods()->add($addData);
    }

    public function getFindGoodsData($goodsCondition)
    {
        return $this->tableGoods()->where($goodsCondition)->find();
    }

    public function getSaveGoodsDataStatus($saveCondition, $saveData)
    {
        return $this->tableGoods()->where($saveCondition)->save($saveData);
    }

    public function getDeleteGoodsDataStatus($deleteId)
    {
        return $this->tableGoods()->delete($deleteId);
    }

    public function getSearchGoodsData($condition)
    {
        import('@.ORG.Page');
        $count = $this->tableGoods()->where($condition)->count();
        $page = new Page($count, 12);
        $page->setConfig('header', '件商品');
        $page->setConfig('theme', '<span class="pagestyle" style="color:blue">共%totalRow%%header%</span> <span class="pagestyle">当前%nowPage%&nbsp;/&nbsp;%totalPage% 页</span> %first% %upPage%  %linkPage% %downPage% %end%');
        $result['list'] = $this->tableGoods()->where($condition)->order('id desc')->limit($page->firstRow.','.$page->listRows)->select();
        $result['show'] = $page->show();
        return $result;
    }

}