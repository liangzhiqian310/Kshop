<?php
/**
 * Created by PhpStorm.
 * User: xujiantao
 * Date: 13-11-28
 * Time: 20:26
 */
class CouponModel extends BaseModel
{
    public function getCouponListData()
    {
        import("@.ORG.Page");
        $count = $this->tableCoupon()->count();
        $page = new Page($count, 10);
        $page->setConfig('header', '个优惠码');
        $page->setConfig('theme', '<span class="pagestyle" style="color:blue">共%totalRow%%header%</span> <span class="pagestyle">当前%nowPage%&nbsp;/&nbsp;%totalPage% 页</span> %first% %upPage%  %linkPage% %downPage% %end%');
        $result['list'] = $this->tableCoupon()->order('id desc')->limit($page->firstRow.','.$page->listRows)->select();
        $result['show'] = $page->show();
        return $result;
    }

    public function getAddCouponDataStatus($addData)
    {
        return $this->tableCoupon()->add($addData);
    }

    public function getFindCouponData($condition)
    {
        return $this->tableCoupon()->where($condition)->find();
    }

    public function getSaveCouponDataStatus($condition, $saveData)
    {
        return $this->tableCoupon()->where($condition)->save($saveData);
    }

    public function getSearchUserIdData($condition)
    {
        return $this->tableUser()->where($condition)->getField('id');
    }

    public function getDeleteCouponDataStatus($deleteId)
    {
        return $this->tableCoupon()->delete($deleteId);
    }

    public function getSearchCouponData($condition)
    {
        import("@.ORG.Page");
        $count = $this->tableCoupon()->where($condition)->count();
        $page = new Page($count,10);
        $page->setConfig('header', '个优惠码');
        $page->setConfig('theme', '<span class="pagestyle" style="color:blue">共%totalRow%%header%</span> <span class="pagestyle">当前%nowPage%&nbsp;/&nbsp;%totalPage% 页</span> %first% %upPage%  %linkPage% %downPage% %end%');
        $result['list'] = $this->tableCoupon()->where($condition)->order('id desc')->limit($page->firstRow.','.$page->listRows)->select();
        $result['show'] = $page->show();
        return $result;
    }
}