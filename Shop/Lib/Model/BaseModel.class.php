<?php
/**
 * Created by PhpStorm.
 * User: xujiantao
 * Date: 13-11-18
 * Time: 21:18
 */
class BaseModel extends RelationModel
{

    protected function tableNews()
    {
        return new Model('News');
    }

    protected function tableSlide()
    {
        return new Model('Slide');
    }

    protected function tableGoods()
    {
        return new Model('Goods');
    }

    protected function tableCart()
    {
        return new Model('Cart');
    }

    protected function tableSort()
    {
        return new Model('Sort');
    }

    protected function tableCoupon()
    {
        return new Model('Coupon');
    }

    protected function tableUser()
    {
        $userModel = new Model('User');

        $userRelation = $userModel->switchModel('Relation');
        $link = array(
            'userinfo'=>array(
                'mapping_type' => HAS_ONE,
                'class_name'   => 'userinfo',
                'mapping_name' => 'userinfo',
                'foreign_key'  => 'user_id',
                'as_fields'    => 'id:userinfo_id,name,gender,birth_date,sel0,sel1,sel2,site,zip_code,mobile,phone,question,answer'
            ),
        );
        $userRelation->setProperty('_link', $link);
        return $userRelation;
    }

    protected function tableUserinfo()
    {
        return new Model('Userinfo');
    }

    protected function tableOrders()
    {
        return new Model('Orders');
    }

    protected function tableComment()
    {
        return new Model('Comment');
    }

    protected function tableShopSet()
    {
        return new Model('Shop_set');
    }

    protected function tableAdmin()
    {
        return new Model('Admin');
    }

    //标题、关键字、描述
    public function webInfo()
    {
        return $this->tableShopSet()->find();
    }

    //网站底部新闻
    public function webFooterNews()
    {
        return $this->tableNews()->order('id asc')->select();
    }

    //商品分类导航
    protected function goodsSort()
    {
        $result['sort_list'] = $this->tableSort()->field('id,name,pid,path,concat(path,"-",id) as bpath')->order('bpath')->select();
        $result['one_list'] = $this->tableSort()->where("pid = 0")->order("id asc")->select();
        return $result;
    }
}